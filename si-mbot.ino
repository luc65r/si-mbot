/*
 * Copyright (c) 2020, Lucas Ransan <lucas@ransan.tk>
 * All rights reserved.
 *
 *   Redistribution and use in source and binary forms, with or without
 *   modification, are permitted provided that the following conditions are met:
 * 
 *   * Redistributions of source code must retain the above copyright notice, this
 *     list of conditions and the following disclaimer.
 *
 *   * Redistributions in binary form must reproduce the above copyright notice,
 *     this list of conditions and the following disclaimer in the documentation
 *     and/or other materials provided with the distribution.
 *
 *     THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 *     AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 *     IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *     DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 *     FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 *     DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 *     SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 *     CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 *     OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 *     OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <MeMCore.h>
#define VITESSE 100

MeDCMotor moteur1(M1); //Moteur de gauche
MeDCMotor moteur2(M2); //Moteur de droite
MeLineFollower ligne(PORT_4); //Détecteur de ligne

void setup() {
}

void loop() {
  //moteur.run() vitesse de -255 à 255, 0 pour arrêté
  int position = ligne.readSensors();
  int i = 0;
  
  switch(position) {
    case S1_IN_S2_IN: //Les deux capteurs détectent la ligne noire
      //On avance tout droit
      moteur1.run(-VITESSE);
      moteur2.run(VITESSE);
      break;
      
    case S1_IN_S2_OUT: //Seulement le capteur de gauche détecte la ligne noire
      //On tourne à gauche
      moteur1.run(0);
      moteur2.run(VITESSE);
      i = 1;
      break;
      
    case S1_OUT_S2_IN: //Seulement le capteur de droite détecte la ligne noire
      //On tourne à droite
      moteur1.run(-VITESSE);
      moteur2.run(0);
      i = 2;
      break;
      
    case S1_OUT_S2_OUT: //Aucun des deux capteurs ne détecte la ligne noire
      //On tourne du côté vers lequel on a tourné précédemment pour éviter le problème au croisement
      switch(i) {
        case 1:
          moteur1.run(0);
          moteur2.run(VITESSE);
          break;
          
        case 2:
          moteur1.run(-VITESSE);
          moteur2.run(0);
          break;
      }
      break;
  }
  delay(10);
}
